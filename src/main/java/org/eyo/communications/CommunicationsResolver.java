package org.eyo.communications;

public interface CommunicationsResolver {
    void send(String value);

    String readLine();

    void close();

    String receiveLine();

    int receiveInt();

    boolean receiveBoolean();
}
