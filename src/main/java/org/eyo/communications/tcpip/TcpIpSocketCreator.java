package org.eyo.communications.tcpip;

import org.apache.log4j.Logger;
import org.eyo.communications.SocketCreator;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class TcpIpSocketCreator implements SocketCreator {

    private static final Logger logger = Logger.getLogger(TcpIpSocketCreator.class);

    @Override
    public Socket createSocket(String host, int port) {
        try {
            return new Socket(host, port);
        } catch (Exception e) {
            logger.error(e);
            return null;
        }
    }

    @Override
    public ServerSocket createServerSocket(int port) {
        try {
            return new ServerSocket(port);
        } catch (IOException ex) {
            logger.error(ex);
            return null;
        }
    }
}
