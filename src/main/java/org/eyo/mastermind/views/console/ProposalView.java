package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.utils.Asker;
import org.eyo.mastermind.utils.ConsoleAsker;
import org.eyo.mastermind.views.ProposalInteractView;

public class ProposalView implements ProposalInteractView {

    private Asker asker;

    public ProposalView() {
        super();
        this.asker = new ConsoleAsker();
    }

    public boolean interact(ProposalUseCaseController proposalController) {
        ProposalMenu proposalMenu = new ProposalMenu(proposalController);
        proposalMenu.setAsker(this.asker);
        proposalMenu.execute();
        return true;
    }

    public void setAsker(Asker asker) {
        this.asker = asker;
    }

}
