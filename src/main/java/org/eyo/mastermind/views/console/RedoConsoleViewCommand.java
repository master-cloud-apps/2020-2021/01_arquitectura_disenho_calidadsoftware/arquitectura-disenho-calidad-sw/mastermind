package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class RedoConsoleViewCommand extends ConsoleViewCommand {
    public RedoConsoleViewCommand(ProposalUseCaseController proposalController) {
        super(MessageView.REDO_COMMAND.getMessage(), proposalController);
    }

    @Override
    protected void execute() {
        this.proposalController.redo();
    }

    @Override
    protected boolean isActive() {
        return this.proposalController.redoable();
    }

}