package org.eyo.mastermind.views.console;

import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.ProposedCombination;
import org.eyo.mastermind.utils.Asker;
import org.eyo.mastermind.utils.ConsoleAsker;
import org.eyo.mastermind.utils.WithConsoleView;

public class ProposedCombinationView extends WithConsoleView {
    private Asker asker;

    public ProposedCombinationView(Asker asker) {
        super();
        this.asker = asker;
    }

    public ProposedCombinationView() {
        this(new ConsoleAsker());
    }

    public Combination readProposedCombination() {
        String sProposedCombination = this.asker.ask(MessageView.PROPOSED_COMBINATION.getMessage());
        Combination proposedCombination = new ProposedCombination(sProposedCombination);
        if (!proposedCombination.check().isCorrect()){
            this.console.writeln(proposedCombination.check().getReason());
            return this.readProposedCombination();
        }
        return proposedCombination;
    }

    public void setAsker(Asker asker) {
        this.asker = asker;
    }
}
