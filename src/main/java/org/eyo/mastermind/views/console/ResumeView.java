package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ResumeUseCaseController;
import org.eyo.mastermind.utils.Asker;
import org.eyo.mastermind.utils.ConsoleAsker;
import org.eyo.mastermind.utils.WithConsoleView;
import org.eyo.mastermind.views.ResumeInteractView;

public class ResumeView extends WithConsoleView implements ResumeInteractView {

    private Asker asker;

    private static final String NEGATIVE = "n";

    public ResumeView() {
        super();
        this.asker = new ConsoleAsker();
    }

    public ResumeView setAsker(Asker asker) {
        this.asker = asker;
        return this;
    }

    public boolean interact(ResumeUseCaseController controller) {
        String resume = this.asker.ask(MessageView.RESUME.getMessage());
        controller.clear(resume != null && !NEGATIVE.equals(resume));
        return resume != null && !NEGATIVE.equals(resume);
    }
}
