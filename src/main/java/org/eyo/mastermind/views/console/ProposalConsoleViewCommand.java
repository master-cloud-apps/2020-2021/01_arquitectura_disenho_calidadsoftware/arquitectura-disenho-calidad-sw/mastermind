package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.utils.Asker;

public class ProposalConsoleViewCommand extends ConsoleViewCommand {

    private SecretCombinationView secretCombinationView;
    private ProposedCombinationView proposedCombinationView;

    public ProposalConsoleViewCommand(ProposalUseCaseController proposalController) {
        super(MessageView.PROPOSE_COMMAND.getMessage(), proposalController);
        this.secretCombinationView = new SecretCombinationView();
        this.proposedCombinationView = new ProposedCombinationView();
    }

    private void printListResults(){
        String results = this.proposalController.getResultsAsString();
        this.console.write(results.replaceAll("\\.", "\n"));
    }

    @Override
    protected void execute() {
        proposalController.addProposedCombination(this.proposedCombinationView.readProposedCombination());
        this.console.writeln();
        this.console.write(Integer.toString(proposalController.getResultsSize()));
        MessageView.ATTEMPTS.writeln();
        this.secretCombinationView.writeEncryptedSecret(proposalController);
        this.printListResults();
        if (proposalController.isWinner()){
            MessageView.WINNER.writeln();
            proposalController.continueState();
        } else if (proposalController.isLooser()) {
            MessageView.LOOSER.writeln();
            proposalController.continueState();
        }
    }

    @Override
    protected boolean isActive() {
        return true;
    }

    public ProposedCombinationView getProposedCombinationView() {
        return proposedCombinationView;
    }

    @Override
    public void setAsker(Asker asker) {
        super.setAsker(asker);
        this.proposedCombinationView.setAsker(asker);
    }
}
