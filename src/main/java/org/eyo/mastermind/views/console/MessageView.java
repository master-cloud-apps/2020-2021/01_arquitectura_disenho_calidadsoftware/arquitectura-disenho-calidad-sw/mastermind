package org.eyo.mastermind.views.console;

import org.eyo.mastermind.utils.Console;

public enum MessageView {
    START_GAME_GREETING("--- MASTERMIND ---"),
    SECRET("*"),
    PROPOSED_COMBINATION("Propose a combination: "),
    UNDO_COMMAND("Undo previous action"),
    PROPOSE_COMMAND("Propose a combination"),
    REDO_COMMAND("Redo previous action"),
    RETURN("\n"),
    ATTEMPTS(" attempt(s): "),
    WINNER("You've won!!! ;-)"),
    LOOSER("You've lost!!! :-("),
    RESUME("RESUME? (Y/n): "),
    CHOOSE_OPTION("Choose an option: ");

    private String message;

    private Console console = Console.instance();

    MessageView(String message) {
        this.message = message;
    }

    void writeln() {
        this.console.writeln(this.message);
    }

    public String getMessage() {
        return message;
    }

    public void write() {
        this.console.write(this.message);
    }
}
