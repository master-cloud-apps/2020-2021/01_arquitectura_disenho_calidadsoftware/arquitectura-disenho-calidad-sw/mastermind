package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.utils.Asker;
import org.eyo.mastermind.utils.ConsoleAsker;

public abstract class ConsoleViewCommand extends org.eyo.mastermind.utils.Command {

    protected ProposalUseCaseController proposalController;
    protected Asker asker;

    protected ConsoleViewCommand(String title, ProposalUseCaseController proposalController) {
        super(title);
        this.proposalController = proposalController;
        this.asker = new ConsoleAsker();
    }

    public void setAsker(Asker asker){
        this.asker = asker;
    }
}
