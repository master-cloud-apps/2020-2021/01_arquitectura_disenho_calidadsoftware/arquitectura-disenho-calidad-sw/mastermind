package org.eyo.mastermind.views;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public interface ProposalInteractView {

    boolean interact(ProposalUseCaseController proposalController);
}
