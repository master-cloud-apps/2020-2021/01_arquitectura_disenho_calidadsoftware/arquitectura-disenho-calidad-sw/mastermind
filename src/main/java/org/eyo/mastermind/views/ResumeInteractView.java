package org.eyo.mastermind.views;

import org.eyo.mastermind.controllers.ResumeUseCaseController;

public interface ResumeInteractView {

    boolean interact(ResumeUseCaseController resumeController);
}
