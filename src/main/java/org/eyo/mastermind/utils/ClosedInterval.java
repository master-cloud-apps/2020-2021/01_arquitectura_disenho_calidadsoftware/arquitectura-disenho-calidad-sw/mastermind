package org.eyo.mastermind.utils;

public class ClosedInterval {

	private int min;
	private int max;

	public ClosedInterval(int min, int max) {
		if (max < min) {
			throw new IllegalArgumentException("Invalid interval. min: " + min + "; max: " + max);
		}
		this.min = min;
		this.max = max;
	}

	public boolean isIncluded(int value) {
		return this.min <= value && value <= this.max;
	}
	
	@Override
	public String toString() {
		return "[" + this.min + ", " + this.max + "]";
	}

}
