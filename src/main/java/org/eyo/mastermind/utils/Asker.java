package org.eyo.mastermind.utils;

public interface Asker {

    String ask(String questionMessage);
}
