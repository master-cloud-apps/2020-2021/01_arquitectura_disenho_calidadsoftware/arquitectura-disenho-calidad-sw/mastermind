package org.eyo.mastermind.models;

import org.eyo.mastermind.types.StateValue;

import java.util.List;

public class Session implements GameSession {

    private Board board;
    private State state;
    private BoardRegistry boardRegistry;

    public Session() {
        this.state = new State();
        this.board = new Board();
        this.boardRegistry = new BoardRegistry(this.board);
    }

    public StateValue getStateValue() {
        return this.state.getValueState();
    }

    public void next() {
        this.state.next();
    }

    public void reset() {
        this.board.reset();
        this.state.reset();
        this.boardRegistry.reset();
    }

    public void addProposedCombination(Combination combination) {
        this.board.addProposedCombination(combination);
        this.registry();
    }

    public Combination getProposedCombination(int position) {
        return this.board.getProposedCombination(position);
    }

    public Combination getSecretCombination() {
        return this.board.getSecretCombination();
    }

    public Result getResult(int position) {
        return this.board.getResult(position);
    }

    public Board getBoard() {
        return board;
    }

    @Override
    public StateValue getValueState() {
        return this.getStateValue();
    }

    public List<Result> getResults() {
        return this.board.getResults();
    }

    public boolean isWinner() {
        return this.board.isWinner();
    }

    public boolean isLooser() {
        return this.board.isLooser();
    }

    public State getState() {
        return this.state;
    }

    public boolean undoable() {
        return this.boardRegistry.undoable();
    }

    public boolean redoable() {
        return this.boardRegistry.redoable();
    }

    public void undo() {
        this.boardRegistry.undo();
    }

    public void redo() {
        this.boardRegistry.redo();
    }

    public void registry() {
        this.boardRegistry.registry();
    }
}
