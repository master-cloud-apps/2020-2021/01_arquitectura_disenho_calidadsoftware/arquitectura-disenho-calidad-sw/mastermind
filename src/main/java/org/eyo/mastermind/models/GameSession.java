package org.eyo.mastermind.models;

import org.eyo.mastermind.types.StateValue;

public interface GameSession {

    StateValue getStateValue();
    Combination getSecretCombination();
    Board getBoard();
    StateValue getValueState();
}
