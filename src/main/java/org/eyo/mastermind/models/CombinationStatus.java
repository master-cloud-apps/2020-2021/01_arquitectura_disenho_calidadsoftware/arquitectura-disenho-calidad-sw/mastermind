package org.eyo.mastermind.models;

/**
 * albertoeyo created on 10/10/2020
 **/
public class CombinationStatus {
    private boolean isCorrect;
    private String reason;

    public boolean isCorrect() {
        return isCorrect;
    }

    public String getReason() {
        return reason;
    }

    public CombinationStatus(boolean isCorrect, String reason) {
        this.isCorrect = isCorrect;
        this.reason = reason;
    }
}
