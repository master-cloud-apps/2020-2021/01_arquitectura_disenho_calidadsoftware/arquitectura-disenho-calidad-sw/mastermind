package org.eyo.mastermind.models;

public class Memento {

    public Board getBoard() {
        return board;
    }

    private Board board;

    Memento(Board board) {
        this.board = board.copy();
    }
}
