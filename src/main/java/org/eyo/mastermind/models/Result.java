package org.eyo.mastermind.models;

/**
 * albertoeyo created on 11/10/2020
 **/
public class Result {

    private String proposedCombination;
    private int blacks;
    private int whites;

    public Result(int blacks, int whites, String proposedCombination) {
        this.blacks = blacks;
        this.whites = whites;
        this.setProposedCombination(proposedCombination);
    }

    public String getProposedCombination() {
        return proposedCombination;
    }

    public void setProposedCombination(String proposedCombination) {
        this.proposedCombination = proposedCombination;
    }

    public int getBlacks() {
        return blacks;
    }

    public void setBlacks(int blacks) {
        this.blacks = blacks;
    }

    public int getWhites() {
        return whites;
    }

    public void setWhites(int whites) {
        this.whites = whites;
    }

    public String getStringResult() {
        return this.getProposedCombination() + " --> " + this.blacks + " blacks and " + this.whites + " whites.";
    }

    public boolean isWinner() {
        return this.blacks == Combination.COMBINATION_SIZE;
    }
}
