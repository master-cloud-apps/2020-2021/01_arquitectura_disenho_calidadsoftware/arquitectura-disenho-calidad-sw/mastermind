package org.eyo.mastermind.controllers;

public interface ControllersVisitor {

    void visit(StartUseCaseController startController);

    void visit(ProposalUseCaseController proposalController);

    void visit(ResumeUseCaseController resumeController);

}
