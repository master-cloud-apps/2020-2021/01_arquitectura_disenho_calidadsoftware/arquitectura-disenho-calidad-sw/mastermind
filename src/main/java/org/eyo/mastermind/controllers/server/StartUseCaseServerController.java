package org.eyo.mastermind.controllers.server;

import org.eyo.mastermind.controllers.StartUseCaseController;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Session;

public class StartUseCaseServerController extends StartUseCaseController {

    private final Session session;

    public StartUseCaseServerController(GameSession sessionImpl) {
        super(sessionImpl);
        this.session = ((Session)this.sessionImpl);
    }

    @Override
    public void start() {
        this.session.next();
    }
}
