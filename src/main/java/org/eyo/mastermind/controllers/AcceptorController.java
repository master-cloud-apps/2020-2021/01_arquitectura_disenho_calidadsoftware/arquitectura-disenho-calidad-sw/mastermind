package org.eyo.mastermind.controllers;

import org.eyo.mastermind.types.StateValue;

public interface AcceptorController {

    void accept(ControllersVisitor controllersVisitor);

    StateValue getValueState();
}
