package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.types.StateValue;

import java.util.EnumMap;

public class Logic {

    protected GameSession sessionImpl;
    protected EnumMap<StateValue, AcceptorController> controllers;

    public Logic(){
        this.controllers = new EnumMap<>(StateValue.class);
    }

    public AcceptorController getController() {
        return this.controllers.get(this.sessionImpl.getStateValue());
    }

    public GameSession getSession() {
        return this.sessionImpl;
    }
}
