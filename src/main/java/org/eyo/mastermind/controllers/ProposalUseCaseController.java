package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.Result;

import java.util.List;

public abstract class ProposalUseCaseController extends UseCaseController implements AcceptorController {

    protected ProposalUseCaseController(GameSession sessionImpl) {
        super(sessionImpl);
    }

    @Override
    public void accept(ControllersVisitor controllersVisitor) {
        controllersVisitor.visit(this);
    }

    public abstract void addProposedCombination(Combination proposedCombination);

    public abstract List<Result> getResults();

    public abstract String getResultsAsString();

    public abstract int getResultsSize();

    public abstract boolean isWinner();

    public abstract boolean isLooser();

    public abstract void continueState();

    public abstract boolean undoable();

    public abstract boolean redoable();

    public abstract void undo();

    public abstract void redo();

}
