package org.eyo.mastermind.controllers;

import org.eyo.mastermind.models.GameSession;

public abstract class ResumeUseCaseController extends UseCaseController implements AcceptorController{
    protected ResumeUseCaseController(GameSession sessionImpl) {
        super(sessionImpl);
    }

    @Override
    public void accept(ControllersVisitor controllersVisitor) {
        controllersVisitor.visit(this);
    }

    public abstract void clear(boolean newGame);

}
