package org.eyo.mastermind.communications;

public interface MastermindCommunicationResolver {
    void close();

    String receiveLine();

    void send(String name);

    void send(int value);

    void send(boolean value);

    void send(char value);

    int receiveInt();

    boolean receiveBoolean();
}
