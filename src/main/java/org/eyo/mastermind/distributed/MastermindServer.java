package org.eyo.mastermind.distributed;

import org.eyo.communications.NetworkFactory;
import org.eyo.communications.tcpip.TcpIpSocketCreator;
import org.eyo.mastermind.communications.NetworkResolver;
import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.distributed.controllers.LogicServer;
import org.eyo.mastermind.distributed.dispatchers.DispatcherPrototype;

public class MastermindServer {
    private final DispatcherPrototype dispatcherPrototype;
    private final LogicServer logic;
    private MastermindCommunicationResolver communicationsResolver;

    public MastermindServer(MastermindCommunicationResolver communicationsResolver) {
        this.communicationsResolver = communicationsResolver;
        this.dispatcherPrototype = new DispatcherPrototype(this.communicationsResolver);
        this.logic = new LogicServer();
        this.logic.createDispatchers(this.dispatcherPrototype);
    }

    public static void main(String[] args){
        NetworkFactory socketNetworkFactory = new NetworkFactory(new TcpIpSocketCreator());
        new MastermindServer(new NetworkResolver(socketNetworkFactory, true)).serve();
    }

    public void serve() {
        this.dispatcherPrototype.serve();
    }
}
