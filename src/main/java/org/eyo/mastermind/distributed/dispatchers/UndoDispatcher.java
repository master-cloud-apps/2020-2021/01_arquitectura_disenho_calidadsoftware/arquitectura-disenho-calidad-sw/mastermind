package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class UndoDispatcher extends Dispatcher {
    public UndoDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        ((ProposalUseCaseController)this.acceptorController).undo();
    }
}
