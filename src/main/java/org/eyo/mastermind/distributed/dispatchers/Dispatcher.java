package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.AcceptorController;
import org.eyo.mastermind.communications.MastermindCommunicationResolver;

public abstract class Dispatcher {

    protected AcceptorController acceptorController;
    protected MastermindCommunicationResolver mastermindCommunicationResolver;

    public Dispatcher(AcceptorController acceptorController) {
        this.acceptorController = acceptorController;
    }

    public void associate(MastermindCommunicationResolver mastermindCommunicationResolver){
        this.mastermindCommunicationResolver = mastermindCommunicationResolver;
    }
    public abstract void dispatch();
}
