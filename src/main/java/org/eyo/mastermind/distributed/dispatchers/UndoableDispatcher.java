package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class UndoableDispatcher extends Dispatcher {
    public UndoableDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        boolean undoable = ((ProposalUseCaseController) this.acceptorController).undoable();
        this.mastermindCommunicationResolver.send(undoable);
    }
}
