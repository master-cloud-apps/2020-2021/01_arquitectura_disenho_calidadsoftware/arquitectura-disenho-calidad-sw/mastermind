package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class GetResultSizeDispatcher extends Dispatcher {
    public GetResultSizeDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        int resultSize = ((ProposalUseCaseController) this.acceptorController).getResultsSize();
        this.mastermindCommunicationResolver.send(resultSize);
    }
}
