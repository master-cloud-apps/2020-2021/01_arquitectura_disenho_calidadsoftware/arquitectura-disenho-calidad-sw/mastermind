package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class IsLooserDispatcher extends Dispatcher {
    public IsLooserDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        boolean isLooser = ((ProposalUseCaseController) this.acceptorController).isLooser();
        this.mastermindCommunicationResolver.send(isLooser);
    }
}
