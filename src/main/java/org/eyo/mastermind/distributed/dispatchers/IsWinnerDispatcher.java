package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class IsWinnerDispatcher extends Dispatcher {
    public IsWinnerDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        boolean isWinner = ((ProposalUseCaseController) this.acceptorController).isWinner();
        this.mastermindCommunicationResolver.send(isWinner);
    }
}
