package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.controllers.ProposalUseCaseController;

public class GetResultsStringDispatcher extends Dispatcher {
    public GetResultsStringDispatcher(ProposalUseCaseController proposalUseCaseController) {
        super(proposalUseCaseController);
    }

    @Override
    public void dispatch() {
        String resultsAsString = ((ProposalUseCaseController) this.acceptorController).getResultsAsString();
        this.mastermindCommunicationResolver.send(resultsAsString);
    }
}
