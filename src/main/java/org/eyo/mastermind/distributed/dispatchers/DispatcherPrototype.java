package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;

import java.util.EnumMap;
import java.util.Map;

public class DispatcherPrototype {
    private MastermindCommunicationResolver communicationResolver;
    private Map<FrameType, Dispatcher> dispatcherMap;

    public DispatcherPrototype(MastermindCommunicationResolver communicationResolver) {
        this.communicationResolver = communicationResolver;
        this.dispatcherMap = new EnumMap<>(FrameType.class);
    }

    public void add(FrameType frameType, Dispatcher dispatcher){
        this.dispatcherMap.put(frameType, dispatcher);
        dispatcher.associate(this.communicationResolver);
    }

    public void serve() {
        FrameType frameType = null;
        do {
            String string = this.communicationResolver.receiveLine();
            frameType = FrameType.parser(string);
            if (frameType != FrameType.CLOSE) {
                this.dispatcherMap.get(frameType).dispatch();
            }
        } while (frameType != FrameType.CLOSE);
        this.communicationResolver.close();
    }
}
