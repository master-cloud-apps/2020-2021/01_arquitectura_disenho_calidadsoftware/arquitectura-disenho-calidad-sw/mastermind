package org.eyo.mastermind.distributed.dispatchers;


public enum FrameType {
    START,
    STATE,
    UNDO,
    REDO,
    UNDOABLE,
    REDOABLE,
    IS_LOOSER,
    IS_WINNER,
    GET_RESULT_SIZE,
    GET_RESULTS_STRING,
    ADD_PROPOSED_COMBINATION,
    CLOSE,
    NEW_GAME,
    COORDINATE_VALID,
    CONTINUE_STATE;


    public static FrameType parser(String string) {
        for(FrameType frameType : FrameType.values()) {
            if (frameType.name().equals(string)) {
                return frameType;
            }
        }
        return CLOSE;
    }
}
