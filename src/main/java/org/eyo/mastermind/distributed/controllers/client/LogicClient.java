package org.eyo.mastermind.distributed.controllers.client;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.controllers.Logic;
import org.eyo.mastermind.distributed.ClientGameSession;
import org.eyo.mastermind.types.StateValue;

public class LogicClient extends Logic {

    private MastermindCommunicationResolver mastermindCommunicationResolver;

    public LogicClient(MastermindCommunicationResolver mastermindCommunicationResolver) {
        this.mastermindCommunicationResolver = mastermindCommunicationResolver;
        this.sessionImpl = new ClientGameSession(this.mastermindCommunicationResolver);
        this.controllers.put(StateValue.INITIAL,
                new StartUseCaseClientController(this.sessionImpl, this.mastermindCommunicationResolver));
        this.controllers.put(StateValue.IN_GAME,
                new ProposalUseCaseClientController(this.sessionImpl, this.mastermindCommunicationResolver));
        this.controllers.put(StateValue.RESUME,
                new ResumeUseCaseClientController(this.sessionImpl, this.mastermindCommunicationResolver));
        this.controllers.put(StateValue.EXIT, null);
    }

}
