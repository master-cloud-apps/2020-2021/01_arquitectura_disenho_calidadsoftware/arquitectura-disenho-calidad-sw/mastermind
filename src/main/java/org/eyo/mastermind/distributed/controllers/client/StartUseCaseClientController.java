package org.eyo.mastermind.distributed.controllers.client;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.controllers.StartUseCaseController;
import org.eyo.mastermind.distributed.dispatchers.FrameType;
import org.eyo.mastermind.models.GameSession;

public class StartUseCaseClientController extends StartUseCaseController {

    private MastermindCommunicationResolver netWorkResolver;

    public StartUseCaseClientController(GameSession session, MastermindCommunicationResolver comResolver) {
        super(session);
        this.netWorkResolver = comResolver;
    }


    @Override
    public void start() {
        this.netWorkResolver.send(FrameType.START.name());
    }
}
