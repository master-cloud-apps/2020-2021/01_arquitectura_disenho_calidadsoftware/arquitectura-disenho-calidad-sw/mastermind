package org.eyo.mastermind.distributed;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.distributed.dispatchers.FrameType;
import org.eyo.mastermind.models.Board;
import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.GameSession;
import org.eyo.mastermind.models.ProposedCombination;
import org.eyo.mastermind.types.StateValue;

public class ClientGameSession implements GameSession {

    private MastermindCommunicationResolver netWorkResolver;

    public ClientGameSession(MastermindCommunicationResolver netWorkResolver) {
        this.netWorkResolver = netWorkResolver;
    }

    @Override
    public StateValue getStateValue() {
        this.netWorkResolver.send(FrameType.STATE.name());
        return StateValue.values()[this.netWorkResolver.receiveInt()];
    }

    @Override
    public Combination getSecretCombination() {
        return new ProposedCombination("bbbb");
    }

    @Override
    public Board getBoard() {
        return null;
    }

    @Override
    public StateValue getValueState() {
        return this.getStateValue();
    }
}
