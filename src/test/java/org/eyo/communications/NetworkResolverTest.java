package org.eyo.communications;

import org.eyo.mastermind.communications.NetworkResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class NetworkResolverTest {

    private NetworkResolver networkClientResolver;
    @Mock
    private NetworkFactory networkFactory;
    @Mock
    private CommunicationsResolver tcpip;
    private NetworkResolver networkServerResolver;

    @BeforeEach
    void setUp(){
        when(this.networkFactory.createCommunicationClientInterface()).thenReturn(this.tcpip);
        when(this.networkFactory.createCommunicationServerInterface()).thenReturn(this.tcpip);
        this.networkClientResolver = new NetworkResolver(this.networkFactory, false);
        this.networkServerResolver = new NetworkResolver(this.networkFactory, true);
    }

    @Test
    void should_get_string(){
        when(this.tcpip.receiveLine()).thenReturn("hello");
        assertEquals("hello", this.networkClientResolver.receiveLine());
        assertEquals("hello", this.networkServerResolver.receiveLine());
    }

    @Test
    void should_get_int(){
        when(this.tcpip.receiveInt()).thenReturn(1);
        assertEquals(1, this.networkClientResolver.receiveInt());
    }

    @Test
    void should_get_boolean(){
        when(this.tcpip.receiveBoolean()).thenReturn(false);
        assertFalse(this.networkClientResolver.receiveBoolean());
    }

    @Test
    void should_call_close(){
        this.networkClientResolver.close();

        verify(this.tcpip).close();
    }

    @Test
    void should_call_send_string(){
        this.networkClientResolver.send("hello");

        verify(this.tcpip).send("hello");
    }

    @Test
    void should_call_send_int(){
        this.networkClientResolver.send(1);

        verify(this.tcpip).send("1");
    }

    @Test
    void should_call_send_boolean(){
        this.networkClientResolver.send(true);

        verify(this.tcpip).send("true");
    }

    @Test
    void should_call_send_char(){
        this.networkClientResolver.send('a');

        verify(this.tcpip).send("a");
    }
}
