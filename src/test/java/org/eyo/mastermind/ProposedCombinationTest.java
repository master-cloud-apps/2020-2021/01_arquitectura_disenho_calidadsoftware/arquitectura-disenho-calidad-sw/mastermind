package org.eyo.mastermind;

import org.eyo.mastermind.models.Combination;
import org.eyo.mastermind.models.ProposedCombination;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.junit.jupiter.api.Assertions.assertEquals;

@DisplayName("Tests for combinations")
class ProposedCombinationTest {

    private final Combination proposedCombination = new ProposedCombination();

    @Test
    @DisplayName("Should check that the combination has the right length")
    void combination_length_3() {
        this.proposedCombination.setCombination("rrr");
        assert !proposedCombination.check().isCorrect();
        assert proposedCombination.check().getReason().equals("Wrong proposed combination length");
    }

    @Test
    @DisplayName("Should check that the combination has the right colors. x is not a color")
    void combination_incorrect_x(){
        this.proposedCombination.setCombination("rxrr");
        assert !proposedCombination.check().isCorrect();
        assertEquals("Wrong colors, they must be: rbygop", proposedCombination.check().getReason());
    }

    @ParameterizedTest
    @ValueSource(strings = {"rarr", "rybgpo", "rybÑ"})
    void test_not_null(String proposedSCombination) {
        this.proposedCombination.setCombination(proposedSCombination);
        assert !proposedCombination.check().isCorrect();
    }

    @Test
    @DisplayName("Should check that the combination has the right colors")
    void combination_correct(){
        this.proposedCombination.setCombination("rrrr");
        assert proposedCombination.check().isCorrect();
        assertEquals("", proposedCombination.check().getReason());
    }


}
