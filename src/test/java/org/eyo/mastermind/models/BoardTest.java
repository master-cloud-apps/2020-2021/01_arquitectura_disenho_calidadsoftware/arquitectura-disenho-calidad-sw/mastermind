package org.eyo.mastermind.models;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class BoardTest {

    private Board board;

    @BeforeEach
    void setUp(){
        this.board = new Board();
    }

    @Test
    void should_get_a_proposed_combination_after_adding_one(){
        this.board.reset();
        Combination combination = new ProposedCombination("rrrr");

        this.board.addProposedCombination(combination);

        assertNotNull(this.board.getProposedCombination(0));
    }

    @Test
    void should_get_the_result_4_blacks_from_a_proposed_combination(){
        this.board.reset();
        Combination combination = new ProposedCombination(this.board.getSecretCombination().getsCombination());
        this.board.addProposedCombination(combination);

        Result result = this.board.getResult(0);
        assertEquals(4, result.getBlacks());
    }

    @ParameterizedTest
    @CsvSource({
            "2, 2, rryy, ryry",
            "2, 0, ppob, pppp",
            "2, 1, rgob, ogyb",
    })
    void should_get_the_result_b_blacks_w_whites_from_h_secret_p_proposed(int blacks, int whites, String secret,
                                                                          String proposed){
        this.board.reset();
        this.board.getSecretCombination().setCombination(secret);

        Combination combination = new ProposedCombination(proposed);
        this.board.addProposedCombination(combination);

        Result result = this.board.getResult(0);
        assertEquals(blacks, result.getBlacks());
        assertEquals(whites, result.getWhites());
    }
}
