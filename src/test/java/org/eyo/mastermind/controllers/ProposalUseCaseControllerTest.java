package org.eyo.mastermind.controllers;

import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.models.ProposedCombination;
import org.eyo.mastermind.models.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ProposalUseCaseControllerTest {

    private ProposalUseCaseController proposalController;

    @BeforeEach
    void setUp(){
        this.proposalController = new ProposalUseCaseServerController(new Session());
    }

    @Test
    void should_have_undoable_redoable_options(){
        assertFalse(this.proposalController.undoable());
        assertFalse(this.proposalController.redoable());
    }

    @Test
    void should_be_undoable_twice_combination_set(){
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination("rrrr"));
        this.proposalController.addProposedCombination(new ProposedCombination("bbbb"));
        this.proposalController.addProposedCombination(new ProposedCombination("bbbb"));

        assertTrue(this.proposalController.undoable());
    }

    @Test
    void should_undo_second_move(){
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination("rrrr"));
        this.proposalController.addProposedCombination(new ProposedCombination("bbbb"));
        this.proposalController.addProposedCombination(new ProposedCombination("bbbb"));
        assertEquals(2, this.proposalController.getResults().size());

        this.proposalController.undo();

        assertFalse(this.proposalController.undoable());
        assertEquals(1, this.proposalController.getResults().size());
    }

    @Test
    void should_undo_second_move_and_then_redo(){
        this.proposalController.getBoard().setSecretCombination(new ProposedCombination("rrrr"));
        this.proposalController.addProposedCombination(new ProposedCombination("bbbb"));
        this.proposalController.addProposedCombination(new ProposedCombination("gggg"));
        assertEquals(2, this.proposalController.getResults().size());
        this.proposalController.undo();
        assertEquals(1, this.proposalController.getResults().size());
        assertFalse(this.proposalController.undoable());
        assertTrue(this.proposalController.redoable());

        this.proposalController.redo();

        assertEquals(2, this.proposalController.getResults().size());
        assertFalse(this.proposalController.redoable());
        assertTrue(this.proposalController.undoable());
    }

}
