package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ProposalUseCaseController;
import org.eyo.mastermind.controllers.server.ProposalUseCaseServerController;
import org.eyo.mastermind.models.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

class UndoConsoleViewCommandTest {

    private UndoConsoleViewCommand undoCommand;
    private ProposalUseCaseController proposalController;

    @BeforeEach
    void setUp(){
        this.proposalController = new ProposalUseCaseServerController(new Session());
        this.undoCommand = new UndoConsoleViewCommand(this.proposalController);
    }

    @Test
    void should_not_be_possible_execute_undo_command(){
        assertFalse(this.undoCommand.isActive());
    }
}
