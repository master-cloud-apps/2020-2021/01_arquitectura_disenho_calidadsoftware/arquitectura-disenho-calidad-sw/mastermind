package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.server.StartUseCaseServerController;
import org.eyo.mastermind.models.Session;
import org.eyo.mastermind.types.StateValue;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StartViewTest {

    private StartView startView;
    private Session sessionImpl;

    @BeforeEach
    void setUp(){
        this.sessionImpl = new Session();
        this.startView = new StartView();
    }

    @Test
    void should_return_start_message(){
        this.startView.interact(new StartUseCaseServerController(this.sessionImpl));
        assertEquals(StateValue.IN_GAME, this.sessionImpl.getStateValue());
    }
}
