package org.eyo.mastermind.views.console;

import org.eyo.mastermind.controllers.ResumeUseCaseController;
import org.eyo.mastermind.controllers.server.ResumeUseCaseServerController;
import org.eyo.mastermind.models.Session;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ResumeViewTest {

    private ResumeView resumeView;
    private ResumeUseCaseController resumeController;
    private Session sessionImpl;

    @BeforeEach
    void setUp(){
        this.sessionImpl = new Session();
        this.resumeController = new ResumeUseCaseServerController(this.sessionImpl);
        this.resumeView = new ResumeView();
    }

    @Test
    void should_get_game_resumed(){
        assertFalse(this.resumeView.setAsker(q -> "n").interact(resumeController));
    }

    @Test
    void should_not_get_game_resumed(){
        assertTrue(this.resumeView.setAsker(q -> "y").interact(resumeController));
    }

    @Test
    void should_get_game_resumed_null(){
        assertFalse(this.resumeView.setAsker(q -> null).interact(resumeController));
    }

    @Test
    void should_not_get_game_resumed_other_string(){
        assertTrue(this.resumeView.setAsker(q -> "jfv sjkfbhv").interact(resumeController));
    }
}
