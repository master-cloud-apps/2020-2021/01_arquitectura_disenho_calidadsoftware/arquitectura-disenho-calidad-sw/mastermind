package org.eyo.mastermind;

import org.eyo.mastermind.utils.ConsoleAsker;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import static org.junit.jupiter.api.Assertions.assertEquals;

/**
 * albertoeyo created on 11/10/2020
 **/
class ConsoleAskerTest {

    private ConsoleAsker consoleAsker;

    @BeforeEach
    void setup(){
        this.consoleAsker = new ConsoleAsker();
    }

    @Test
    void should_check_asked_combination(){
        String input = "add 5";
        InputStream in = new ByteArrayInputStream(input.getBytes());
        System.setIn(in);
        String result = this.consoleAsker.ask("Hello: ");
        assertEquals("add 5", result);
    }
}
