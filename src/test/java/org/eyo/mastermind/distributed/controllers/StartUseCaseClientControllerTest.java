package org.eyo.mastermind.distributed.controllers;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.eyo.mastermind.distributed.controllers.client.StartUseCaseClientController;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class StartUseCaseClientControllerTest {

    @InjectMocks
    private StartUseCaseClientController startUseCaseClientController;

    @Mock
    private MastermindCommunicationResolver communicationResolver;

    @Test
    void should_call_start(){
        this.startUseCaseClientController.start();
        verify(this.communicationResolver).send(anyString());
    }
}
