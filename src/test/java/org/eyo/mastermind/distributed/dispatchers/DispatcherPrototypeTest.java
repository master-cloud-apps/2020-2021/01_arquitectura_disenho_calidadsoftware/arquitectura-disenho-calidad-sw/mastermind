package org.eyo.mastermind.distributed.dispatchers;

import org.eyo.mastermind.communications.MastermindCommunicationResolver;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.stubbing.Answer;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@RunWith(JUnitPlatform.class)
class DispatcherPrototypeTest {

    @InjectMocks
    private DispatcherPrototype dispatcherPrototype;
    @Mock
    private MastermindCommunicationResolver communication;

    @BeforeEach
    void setUp(){
        this.dispatcherPrototype.add(FrameType.CLOSE, new Dispatcher(null) {
            @Override
            public void dispatch() {
                System.out.println("Close");
            }
        });
        this.dispatcherPrototype.add(FrameType.START, new Dispatcher(null) {
            @Override
            public void dispatch() {
                System.out.println("Start");
            }
        });
    }

    @Test
    void should_execute_dispatch(){
        when(this.communication.receiveLine()).then(new Answer<String>() {
            private int times = 0;
            @Override
            public String answer(InvocationOnMock invocationOnMock) throws Throwable {
                if (times == 0){
                    times++;
                    return FrameType.START.toString();
                }
                return  FrameType.CLOSE.toString();
            }
        });
        this.dispatcherPrototype.serve();

        verify(this.communication).close();
    }
}
